import string
import sys
translator = str.maketrans('', '', string.punctuation)
count = {}

f = open("mbox-short.txt")

for line in sys.stdin:
	words = line.upper().translate(translator).split()
	for w in words:
		count[w] = count.get(w,0)+1
		
n = sorted(count.items(), key=lambda x: x[1], reverse=True)
for k,v in n:
	print (k,v)
